var config = {};

config.db = {};
config.db.host = "localhost";
config.db.user = "root";
config.db.password = "";
config.db.database = "colegio_node";

//Configuracion para postgress
config.db.pg={}
config.db.pg.host="localhost";
config.db.pg.port=5432;
config.db.pg.user = "ricoru";
config.db.pg.password = "";
config.db.pg.database = "colegiodb";

config.port = 3000;
config.root = "/var/www/colegio-registro-asistencia/";


module.exports.config = config;
module.exports.config.db = config.db;
module.exports.config.db.pg = config.db.pg;
