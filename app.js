var config = require("./config").config;
var jade = require('jade');
var pg = require('pg').native;
var moment = require('moment');

var express = require("express"), 
http = require("http"),
app = express(),
server = http.createServer(app),
path = require('path');


app.engine('html', require('ejs').renderFile);

app.use(express.static(path.join(__dirname, 'public')));
app.set("views",__dirname + "/views");

app.set('view engine', 'jade');

app.configure(function(){
    app.use(express.bodyParser({uploadDir:'./public'}));
    app.use(express.multipart());
    app.use(express.cookieParser());//necesario para utilizar sesiones
	app.use(express.session({cookie: {maxAge: 900000}, secret:"wando"}));//tiempo de expiración de la sesión
	app.use(express.static(__dirname));
});


process.env.TZ = 'America/Lima';

app.get("/register", function(req, res){
    res.render("registro",{msj:""});
});
app.post("/register", function(req, res){
    console.log( req.body.codigo );
    console.log( req.body.fecha );
    if(moment(Number(req.body.fecha)).isValid){
        //res.render("registro",{msj:"Fecha Invalida"});
        res.redirect("/register");
    }{
        client.query("INSERT INTO asistencia(codigo,fecha) values($1, $2)",[req.body.codigo,req.body.fecha]);
        //res.redirect("/register");
        res.send("Registro Correcto");
    }
    //query.on('end', function() { client.end(); });
    //client.end();
    
    /*query.on("row", function (row, result) {
        result.addRow(row);
    });
    query.on("end", function (result) {
        console.log(JSON.stringify(result.rows, null, "    "));
        client.end();
    });*/
    //res.send("");
});
app.get("/listar", function(req, res){
    var results = [];
    var i=1;
    var query = client.query("select al.nombres,asis.fecha from asistencia as asis inner join alumno al on al.codigointerno = asis.codigo");
    query.on("row", function (row, result) {
        var fil_row = [];
        fil_row[0] = i;
        fil_row[1] = row.nombres;
        fil_row[2] = moment(Number(row.fecha)).format("DD-MM-YYYY");
        results.push(fil_row);
        i++;
    });
    query.on("end", function (result) {        
        //console.log(JSON.stringify(result.rows, null, "    "));
        //client.end();
        //return res.json(results);
        console.log(results);
        res.render("listar.jade",{resultado:results});
    });
});


/*FORMULARIO ALUMNO*/
app.get("/alumno", function(req, res){
    res.render("alumno");
});
app.post("/alumno", function(req, res){
    console.log( req.body.codigointerno );
    console.log( req.body.codigomatricula );
    console.log( req.body.nombres );
    console.log( req.body.aula );
    if(req.body.codigointerno!="" && req.body.codigomatricula!="" && req.body.nombres!="" && 
        req.body.aula!="" ){
        res.redirect("/alumno");
    }{
        client.query("INSERT INTO alumno(codigomatricula,codigointerno,nombres,aula) values($1,$2,$3,$4)",
            [req.body.codigomatricula,req.body.codigointerno,req.body.nombres,req.body.aula]);
        res.send("Registro Correcto");
    }listar
});

app.get("/listar-alumno", function(req, res){
    var results = [];
    var i=1;
    var query = client.query("select * from alumno");
    query.on("row", function (row, result) {
        var fil_row = [];
        fil_row[0] = i;
        fil_row[1] = row.codigomatricula;
        fil_row[2] = row.codigointerno;
        fil_row[3] = row.nombres;
        fil_row[4] = row.aula;
        results.push(fil_row);
        i++;
    });
    query.on("end", function (result) {        
        //console.log(JSON.stringify(result.rows, null, "    "));
        //client.end();
        //return res.json(results);
        console.log(results);
        res.render("listar-alumno",{resultado:results});
    });
});


console.log("PUERTO "+config.port);
server.listen(config.port);

/*configuración servidor de base de datos*/
var conString = "postgres://"+config.db.pg.user+":"+config.db.pg.password+"@localhost:"+config.db.pg.port+"/"+config.db.pg.database;

var client = new pg.Client(conString);
client.connect(function(err) {
  if(err) {
    return console.error('could not connect to postgres', err);
  }
});

/*otras funciones javascript ... dejaré de momento por aquí*/
function getLocalData(date){
	var format="";
    format+=(date.getUTCFullYear()+"-");
    format+=(twoDigits(1 + date.getUTCMonth())+"-");
    format+=(twoDigits(date.getUTCDate())+" ");
    format+=(twoDigits(date.getHours())+":");
    format+=(twoDigits(date.getMinutes())+":");
    format+=(twoDigits(date.getSeconds()));
	return format;
}

function twoDigits(d) {
	return (0 <= d && d < 10)?"0" + d.toString():d.toString();
}